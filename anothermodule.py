#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random


size=100

#this is one of two of the main functions. main_MAGN_and_SUSC makes the lattice go through the metropolis algorithm a specified number of times, and does this for an array of temperatures. The magnetisation and magnetic suceptibility are plotted after this number of counts has been applied to each temperature.
def main_magn_and_susc(matrix):
	
	J = 1.0 
	h = 0
	T = 1
	k = 1 
	n = 0.0
	

	#for triangular case, the nearest neighbour terms change every second row, this is because it is applied to a square lattice instead of an actual triangular lattice. Example. First row is moving to the right relative to the second row, but the second row is moving to the left relative to the first row etc etc. So all even rows have the same interaction and all odd rows have the same interaction.
	def deltaE(x,y):
		if x%2==0:
			return -2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y] + matrix[(x+1) %size, (y+1)%size] + matrix[(x+1)%size,(y-1)%size])+2*h*matrix[x,y]
		else:
			return -2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y] + matrix[(x-1) %size, (y+1)%size] + matrix[(x-1)%size,(y-1)%size])+2*h*matrix[x,y]

#Ising function that applies the metropolis alogirthm counts times at temperature T
	def Ising(counts,T):
		for n in range (counts):
			n += 1
		    	for y in range(0, size):
		    		for x in range(0,size):
		    			if deltaE(x,y)>0: 
		    				matrix[x,y] *= -1
		    			else:
		    				r = random.random()
		    				if r<np.exp((deltaE(x,y))/(k*T)):		
		    					matrix[x,y] *= -1 
		

		
		return matrix

	temperaturearray=np.arange(0.1,6.1,0.1)

	#getting the magnetisation of a matrix
	def magnetisation(matrix):
		return (np.abs(np.sum(matrix)))/float(size**2.0)


	for T in temperaturearray:
		matrixformagnetisation=Ising(int(-239*(T**2) + 1461*T -93),T)
		mag=magnetisation(matrixformagnetisation)
		print T
		plt.figure(1)
		plt.title("Graph of Magnetisation vs Temperature")
		plt.plot(T,mag, 'bo')
		plt.xlabel("Temperature")
		plt.ylabel("Magnetisation")
		plt.figure(2)		

		plt.plot(T, (1-mag**2)/(k*T), 'bo')
		plt.title("Graph of Magnetic susceptiblity vs Temperature")
		plt.xlabel("Temperature")
		plt.ylabel("Magnetic Susceptibility")
	plt.show()

	












#similar to the other MAIN function, but instead plots the energy and the heat capacity
def main_ener_and_hcap(matrix):
	J = 1.0 
	h = 0
	T = 1
	k = 1 
	n = 0.0
	def deltaE(x,y):
		if x%2==0:
			return -2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y] + matrix[(x+1) %size, (y+1)%size] + matrix[(x+1)%size,(y-1)%size])+2*h*matrix[x,y]
		else:
			return -2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y] + matrix[(x-1) %size, (y+1)%size] + matrix[(x-1)%size,(y-1)%size])+2*h*matrix[x,y]
	def Ising(counts,T):
		for n in range (counts):
			n += 1
		    	for y in range(0, size):
		    		for x in range(0,size):
		    			if deltaE(x,y)>0: 
		    				matrix[x,y] *= -1 
		    			else:
		    				r = random.random()
		    				if r<np.exp((deltaE(x,y))/(k*T)):		
		    					matrix[x,y] *= -1
		return matrix




	temperaturearray=np.arange(0.1,6.1,0.1)



	def Hamiltonian(x,y):
		return -((J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y]))+h*matrix[x,y])
	    

	def energy(matrix):
		e=0
		for y in range(0, size):
			for x in range(0,size):
				e=e+0.5*Hamiltonian(x,y)
		return e/(size)**2



	def Hamiltonian_sq(x,y):
		return (-((J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y]))+h*matrix[x,y]))**2



	def energy_sq(matrix):
		e_sq=0
		for y in range(0, size):
			for x in range(0,size):
				e_sq=e_sq+0.25*Hamiltonian_sq(x,y)
		return e_sq/size**2

#plotting energy and heat capacity
	for T in temperaturearray:
		matrixforenergy=Ising(int(-239*(T**2) + 1461*T -93),T)
		ener=energy(matrixforenergy)
	    
		matrixforenergy_sq=Ising(int(-239*(T**2) + 1461*T -93),T)
		ener_sq=energy_sq(matrixforenergy_sq)
		plt.figure(3)
		plt.title("Graph of heat capacity vs Temperature")
		plt.xlabel("Temperature")
		plt.ylabel("Heat capacity")
		plt.plot(T,(ener_sq-(ener**2))/(k*T**2), 'bo') 
		plt.figure(4)
		plt.title("Graph of energy vs Temperature")
		plt.xlabel("Temperature")
		plt.ylabel("Energy")
		plt.plot(T, ener, 'bo')
		print T
	plt.show()






#function that displays the initial matrix and the final matrix
def displaylattice(matrix, counts, T):
	J = 1.0 
	h = 0
	k = 1 
	n = 0.0
	initialmatrix=matrix
	plt.imshow(initialmatrix, interpolation='none')
	def deltaE(x,y):
		if x%2==0:
			return -2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y] + matrix[(x+1) %size, (y+1)%size] + matrix[(x+1)%size,(y-1)%size])+2*h*matrix[x,y]
		else:
			return -2*J*matrix[x,y]*(matrix[x, (y+1) %size]+matrix[x, (y-1) %size] + matrix[(x+1) %size, y]  + matrix[(x-1) %size, y] + matrix[(x-1) %size, (y+1)%size] + matrix[(x-1)%size,(y-1)%size])+2*h*matrix[x,y]

	for n in range (counts):
		n += 1
		for y in range(0, size):
	 		for x in range(0,size):
		    		if deltaE(x,y)>0: 
		    			matrix[x,y] *= -1
		    		else:
		    			r = random.random()
		    			if r<np.exp((deltaE(x,y))/(k*T)):		
						matrix[x,y] *= -1 
		
	plt.imshow(matrix, interpolation='none')
	plt.show()
		


