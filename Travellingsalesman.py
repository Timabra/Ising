#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random as random

N=500 #number of cities
counts=1000 #number of counts through the algorithm

k=1
T=1
n=0
array=[]


#creation of N random cities within a square of size 1x1
for n in range(0,N):
	n=n+1
	y=np.random.randint(10001,size=2)
	y=float(y/10000)
	array.append(y)


#function that finds the distance of going through from city 1 to city N
def distance(array):
	dist=0
	counter=0
	for counter in range (0,n):
		dist=dist+np.sqrt((array[counter][1]-array[counter-1][1])**2+(array[counter][0]-array[counter-1][0])**2)
		counter += 1
	return dist
#print distance(array)

distancesgraph=[]
countergraph=[]


counter2=0

#algorithm
for counter2 in range (0,counts):
	counter2=counter2+1
	d0=distance(array)
	x=random.randint(0,n-1)
	y=random.randint(0,n-1)
	#print x,y
	#print array[x], array[y]
	array[x], array[y]=array[y], array[x]
	d1=distance(array)
	print counter2, d0
	distancesgraph.append(d0)
	countergraph.append(counter2)
	if d1>d0:
		r=random.random()
		
		if r<np.exp(d0-d1/k*T):
			array[x],array[y]=array[x],array[y]
		else:
			array[x],array[y]=array[y],array[x]
	
	
#plotting
plt.title('Graph of decreasing distance with counts')
plt.xlabel('counts')
plt.ylabel('distance')
plt.plot(countergraph, distancesgraph)
plt.show()



