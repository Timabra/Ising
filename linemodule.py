#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random


size=100

#this is one of two of the main functions. main_MAGN_and_SUSC makes the lattice go through the metropolis algorithm a specified number of times, and does this for an array of temperatures. The magnetisation and magnetic suceptibility are plotted after this number of counts has been applied to each temperature.
def main_magn_and_susc(matrix,counts1):
	J = 1.0 
	h = 0
	T = 1
	k = 1 
	n = 0.0
	def deltaE(x):
		return -2*J*matrix[x]*(matrix[(x+1)%size]+matrix[(x-1)%size])+2*h*matrix[x]

	def Ising(counts,T):#Ising function that applies the metropolis alogirthm counts times at temperature T
		for n in range (counts):
			n += 1
	
		    	for x in range(0,size):
		    		if deltaE(x)>0: 
		    				matrix[x] *= -1
		    		else:
		    			r = random.random()
		    			if r<np.exp((deltaE(x))/(k*T)):		
		  				matrix[x] *= -1 
		

		
		return matrix


	temperaturearray=np.arange(0.1,4.1,0.1)

	#getting the magnetisation of a matrix
	def magnetisation(matrix):
		return (np.abs(np.sum(matrix)))/float(size**2.0)

	#getting graphs of magnetisation and magnetic susceptibility
	for T in temperaturearray:
		matrixformagnetisation=Ising(counts1,T)
		mag=magnetisation(matrixformagnetisation)
		print T
		plt.figure(1)
		plt.title("Graph of Magnetisation vs Temperature for square lattice")
		plt.plot(T,mag, 'bo')
		plt.xlabel("Temperature")
		plt.ylabel("Magnetisation")
		plt.figure(2)		

		plt.plot(T, (1-mag**2)/(k*T), 'bo')
		plt.title("Graph of Magnetic susceptiblity vs Temperaturefor square lattice")
		plt.xlabel("Temperature")
		plt.ylabel("Magnetic Susceptibility")
	plt.show()

	












#similar to the other MAIN function, but instead plots the energy and the heat capacity
def main_ener_and_hcap(matrix,counts1):
	J = 1.0
	h = 0
	T = 1
	k = 1 
	counts = 30
	n = 0.0
	def deltaE(x):
		return -2*J*matrix[x]*(matrix[(x+1)%size]+matrix[(x-1)%size])+2*h*matrix[x]
	def Ising(counts,T):
		for n in range (counts):
			n += 1
		    	for y in range(0, size):
		    		for x in range(0,size):
		    			if deltaE(x)>0: 
		    				matrix[x] *= -1 
		    			else:
		    				r = random.random()
		    				if r<np.exp((deltaE(x))/(k*T)):		
		    					matrix[x] *= -1
		return matrix




	temperaturearray=np.arange(0.1,4.1,0.1) #must start from 0 to avoid outliers
	def Hamiltonian(x):
		return -(J*matrix[x]*(matrix[(x+1)%size]+matrix[(x-1)%size]))+h*matrix[x]
	    
	def energy(matrix):
		e=0 #initialise energy
		for x in range(0,size):
			e=e+0.5*Hamiltonian(x) #increments energy 
		return e/size**2


	def Hamiltonian_sq(x):
		return (-((J*matrix[x]*(matrix[(x+1)%size]+matrix[(x-1)%size]))+h*matrix[x]))**2

	def energy_sq(matrix):
		e_sq=0
		for x in range(0,size):
			e_sq=e_sq+0.25*Hamiltonian_sq(x)
		return e_sq/size**2

#plotting energy and heat capacity
	for T in temperaturearray:
		matrixforenergy=Ising(counts1,T)
		ener=energy(matrixforenergy)

	    
		matrixforenergy_sq=Ising(counts1,T)
		ener_sq=energy_sq(matrixforenergy_sq)
		plt.figure(3)
		plt.title("Graph of heat capacity vs Temperaturefor for square lattice")
		plt.xlabel("Temperature")
		plt.ylabel("Heat capacity")
		plt.plot(T,(ener_sq-(ener**2))/(k*T**2), 'bo') 
		plt.figure(4)
		plt.title("Graph of energy vs Temperature for square lattice")
		plt.xlabel("Temperature")
		plt.ylabel("Energy")
		plt.plot(T, ener, 'bo')
		print T
	plt.show()









