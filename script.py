#!/usr/bin/env python
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import random
import module as twod #module that deals with 2D square lattice case
import anothermodule as twodtriangular #module that deals with 2D triangular case
import linemodule as ln #module that deals with line case
import threedmodule as threed #module that deals with 3D case

#size of matrix
size=100

#creation of lattices/lines/3d matrix through classes
class Ising_lattice: 
	def __init__(self,size):
		self.size=size
		self.matrix_lattice()
		self.onedimensional()
		self.lowtemplattice()
		self.threedimensional()
	
	def matrix_lattice(self):
		self.lattice = np.random.choice([-1,1], (size,size))#hightemp lattice

	def lowtemplattice(self):
		self.lowtemplattice=np.zeros((size,size), dtype=int)
		for x in range(size):
			for y in range(size):
				self.lowtemplattice[x,y]=1#lowtemp lattice

	def onedimensional(self):
		self.line = np.random.choice([-1,1], (size))#hightemp

	def threedimensional(self):
		self.threedimensionallattice=np.random.choice([-1,1], (size,size,size))#hightemp


lattice1=Ising_lattice(size)  
     
hotmatrix=lattice1.lattice
coldmatrix=lattice1.lowtemplattice
line=lattice1.line
cube=lattice1.threedimensionallattice

T=5 #temperature
counts =100 #change number of counts through the metropolis algorithm where applicable. In some case quadratic in terms of temperature used to determine counts instead.



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#2D square lattice

print twod.main_magn_and_susc(hotmatrix)#this is one of two of the main functions. main_MAGN_and_SUSC makes the lattice go through the metropolis algorithm a number of times that is specified by the quadratic that depends on the temperature int(-402*(T**2) + 1853*T -131), and does this for an array of temperatures. The magnetisation and magnetic suceptibility are plotted versus temperature after this has been applied to each temperature.

#print twod.main_ener_and_hcap(hotmatrix)#This main function does the same, except plots the energy and the heat capacity graphs

#print twod.displaylattice(hotmatrix,counts, T)#This function makes a matrix go through the metropolis algorithm a specificied number of times and returns the initial matrix and the final matrix, when a certain count number is specified
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#2D triangular lattice
#this is now repeated but for the twod triangular lattice
#print twodtriangular.main_magn_and_susc(hotmatrix)

#print twodtriangular.main_ener_and_hcap(hotmatrix)

#print twodtriangular.displaylattice(hotmatrix,counts, T)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#1D case
#print ln.main_magn_and_susc(line,counts)

#print ln.main_ener_and_hcap(line,counts)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#same for 3D case
#print threed.main_magn_and_susc(cube,counts)

#print threed.main_ener_and_hcap(cube, counts)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
